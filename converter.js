var axios = require('axios');

var keys = [
  'title',
  'notes',
  'yield',
  'url',
  'time',
  'status',
  'source',
  'photo_url',
  'instructions',
  'ingredients',
  'description',
  'credits'
];

axios.get('https://trecipes-parse.herokuapp.com/parse/classes/Recipe', {
    headers: {
      'X-Parse-Application-Id': 'w7vF7JDnHvOxUeBRbYTwSp9vFpH58J6fXe50tLN6',
      'X-Parse-Master-Key': PARSE_MASTER_KEY
    }
  })
  .then(function (response) {
    var results = response.data.results;

    results.forEach(function(result) {
      var curACL = result.ACL;
      var recipeObj = JSON.parse(result.recipeObj);
      var saveObj = {};

      // Copy all contents
      keys.forEach(function (key) {
        saveObj[key] = recipeObj[key];
      });
      saveObj.ACL = curACL;


      console.log(saveObj.title);
  
      axios({
        method: 'post',
        url: 'https://trecipes-parse.herokuapp.com/parse/classes/Recipe_v2',
        headers: {
          'X-Parse-Application-Id': 'w7vF7JDnHvOxUeBRbYTwSp9vFpH58J6fXe50tLN6',
          'X-Parse-Master-Key': 'UodFnhAdRlWwfGAojhGN1w7Gx4u79TRZPW2zJapQ'
        },
        data: saveObj
      });
    });
  })
  .catch(function (error) {
    console.log(error);
  })
