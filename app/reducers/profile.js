import {
    SIGNUP_REQUEST, 
    SIGNUP_SUCCESS, 
    SIGNUP_ERROR,
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    CACHED_USER,
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS
} from '../actions/types';
import * as status from '../constants/statuses';

const initialState = {
  loginStatus: status.NOT_REQUESTED,
  curUser: null
};

export default function (state = initialState, action) {
  switch (action.type) {
  case SIGNUP_REQUEST:
    return {...state};
  case SIGNUP_SUCCESS:
    return {...state};
  case SIGNUP_ERROR:
    return {...state};
  case LOGIN_REQUEST:
    return {...state, loginStatus: status.REQUESTED};
  case LOGIN_SUCCESS:
    return {...state, loginStatus: status.SUCCESS, curUser: action.payload, logoutStatus: status.NOT_REQUESTED};
  case LOGIN_ERROR:
    return {...state, loginStatus: status.ERROR};
  case CACHED_USER:
    return {...state, loginStatus: status.SUCCESS, curUser: action.payload, logoutStatus: status.NOT_REQUESTED};
  case LOGOUT_REQUEST:
    return {...state, logoutStatus: status.REQUESTED};
  case LOGOUT_SUCCESS:
    return {...state, logoutStatus: status.SUCCESS, curUser: null, loginStatus: status.NOT_REQUESTED}
  default:
    return state;
  }
}
