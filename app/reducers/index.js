import { combineReducers } from 'redux';
import recipesReducer from './recipes';
import profileReducer from './profile';
import parserReducer from './parser';
import { reducer as formReducer } from 'redux-form';


const rootReducer = combineReducers({
  form: formReducer,
  parser: parserReducer,
  profile: profileReducer,
  recipes: recipesReducer
});

export default rootReducer;
