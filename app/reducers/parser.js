import {
    FETCH_URL_REQUEST, 
    FETCH_URL_SUCCESS, 
    FETCH_URL_ERROR,
    PARSE_IMAGE_REQUEST,
    PARSE_IMAGE_SUCCESS,
    PARSE_IMAGE_ERROR
} from '../actions/types';
import * as status from '../constants/statuses';

const initialState = {
  fetchUrlStatus: status.NOT_REQUESTED,
  parsed: {}
};

export default function (state = initialState, action) {
  switch (action.type) {
  case FETCH_URL_REQUEST:
    return {...state, fetchUrlStatus: status.REQUESTED};
  case FETCH_URL_SUCCESS:
    return {...state, fetchUrlStatus: status.SUCCESS};
  case FETCH_URL_ERROR:
    return {...state, fetchUrlStatus: status.ERROR};
  case PARSE_IMAGE_REQUEST:
    return {...state, parseImageStatus: status.REQUESTED};
  case PARSE_IMAGE_SUCCESS:
    return {...state, parseImageStatus: status.SUCCESS};
  case PARSE_IMAGE_ERROR:
    return {...state, parseImageStatus: status.ERROR};
  default:
    return state;
  }
}
