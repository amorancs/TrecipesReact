import {
    GET_RECIPES_REQUEST, 
    GET_RECIPES_SUCCESS, 
    GET_RECIPES_ERROR,
    SAVE_RECIPE_REQUEST,
    SAVE_RECIPE_SUCCESS,
    SAVE_RECIPE_ERROR
} from '../actions/types';

const initialState = {recipeList: []};
const recipeKeys = [
  'categories',
  'notes',
  'title', 
  'time', 
  'instructions', 
  'ingredients', 
  'photo_url',
  'yield',
  'url',
  'status',
  'credits',
  'source',
  'description'
];

export default function (state = initialState, action) {
  switch (action.type) {
  case GET_RECIPES_REQUEST:
    return {...state};
  case GET_RECIPES_SUCCESS: {
    let results = [];

    action.payload.forEach((recipe) => {
      let curRecipe = {};
      recipeKeys.forEach((key) => {
        curRecipe[key] = recipe.get(key);
      });
      curRecipe['objectId'] = recipe.id;
      results.push(curRecipe);
    });

    return {...state, recipeList: results};
  }
  case GET_RECIPES_ERROR:
    return {...state};
  case SAVE_RECIPE_REQUEST:
    return {...state};
  case SAVE_RECIPE_SUCCESS:
    return {...state};
  case SAVE_RECIPE_ERROR:
    return {...state};
  default:
    return state;
  }
}
