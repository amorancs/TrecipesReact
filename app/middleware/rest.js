
/* eslint-disable no-unused-vars */
export default function (client) {
  const xhrClient = client();

  return ({dispatch, getState}) => next => action => {
    const {xhr, types, ...additional} = action;

    if (!xhr) {
      return next(action);
    } else {
      const [REQUEST, SUCCESS, ERROR] = types;

      next({...additional, type: REQUEST});

      return xhrClient.request(xhr)
                .then((result) => {
                  next({...additional, result, type: SUCCESS});
                })
                .catch((error) => {
                  next({...additional, error, type: ERROR});
                });
    }
  };
}
/* eslint-enable no-unused-vars */
