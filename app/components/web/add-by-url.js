import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import FlatButton from 'material-ui/FlatButton';

import Navigation from './navigation';
import * as parserActions from '../../actions/parser';
import * as status from '../../constants/statuses';
import './add-by-url.css';

const defaultState = {
  isLoading: false,
  isError: false
};

class AddByUrl extends Component {

  componentWillMount() {
    this.setState(defaultState);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.parser.fetchUrlStatus === status.REQUESTED &&
      nextProps.parser.fetchUrlStatus === status.SUCCESS) {

      this.context.router.push('/');
    }
  }
  
  handleSearch(values) {
    console.log('Search URL: ' + values.url);
    this.props.fetchRecipeAtUrl(values.url);
    this.setState({isLoading: true});
  }

  render() {
    return (
      <div>
        <Navigation title="Add Recipe by URL" backLink={true} />
        {this.state.isLoading ? 'Loading...' :  null}
        {this.state.isError ? 'Error!' : null}
        <div className="addByUrlForm-wrapper">
          <form onSubmit={this.props.handleSubmit(this.handleSearch.bind(this))}>
            <div>
              <Field 
                name="url" 
                component={TextField}
                hintText="URL"
                floatingLabelText="URL" 
                type="url"/>
            </div>
            <FlatButton 
              label="Fetch" 
              primary={true}
              type="submit" />
          </form>
        </div>
      </div>
    );
  }
}

AddByUrl.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  fetchRecipeAtUrl: PropTypes.func.isRequired
};

AddByUrl.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps(state) {
  return {
    parser: state.parser
  };
}

export default connect(mapStateToProps, {...parserActions})(reduxForm({form: 'addByUrl'})(AddByUrl));
