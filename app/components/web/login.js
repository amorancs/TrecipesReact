import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';

import * as profileActions from '../../actions/profile';
import * as status from '../../constants/statuses';
import './login.css';

class Login extends Component {

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.loginStatus === status.SUCCESS) {
      this.context.router.push('/');
    }
  }

  handleLogin(values) {
    this.props.processLogin(values.email, values.password);
  }

  render() {
    if (this.props.profile.loginStatus === status.ERROR) {
      return (
        <div>Error logging in.</div>
      );
    }

    if (this.props.profile.loginStatus === status.REQUESTED) {
      return (
        <div className="loginForm-wrapper">
          <CircularProgress />
        </div>
      );
    }

    return (
      <div className="loginForm-wrapper">
        <form onSubmit={this.props.handleSubmit(this.handleLogin.bind(this))}>
          <div>
            <Field 
              name="email" 
              component={TextField}
              hintText="Email"
              floatingLabelText="Email" 
              type="email"/>
          </div>
          <div>
            <Field 
              name="password" 
              component={TextField}
              hintText="Password"
              floatingLabelText="Password" 
              type="password"/>
          </div>
          <FlatButton 
            label="Login" 
            primary={true}
            type="submit" />
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  processLogin: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired
};

Login.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps(state) {
  return {
    profile: state.profile
  };
}

export default connect(mapStateToProps, {...profileActions})(reduxForm({form: 'login'})(Login));
