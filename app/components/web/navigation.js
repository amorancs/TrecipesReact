import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Parse from 'parse';
import AppBar from 'material-ui/AppBar';
import Divider from 'material-ui/Divider';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import HomeIcon from 'material-ui/svg-icons/action/home';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import InfoIcon from 'material-ui/svg-icons/action/info';
import GroceryIcon from 'material-ui/svg-icons/maps/local-grocery-store';
import LogoutIcon from 'material-ui/svg-icons/action/exit-to-app';

import * as profileActions from '../../actions/profile';
import * as status from '../../constants/statuses';
import './navigation.css';

const defaultState = {
  drawerIsOpen: false
};

class Navigation extends Component {

  componentWillMount() {
    this.setState(defaultState);

    if (!this.props.profile.curUser) {
      this.context.router.push('login');
    } 
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.logoutStatus === status.SUCCESS) {
      console.log('navigating to log in page');
      this.context.router.push('login');
    }
  }

  handleLeftIconClick() {
    if (this.props.backLink) {
      //navigate to back link
    } else {
      this.setState({drawerIsOpen: !this.state.drawerIsOpen});
    }
  }

  handleLogoutClick() {
    this.props.processLogout();
  }

  handleSettingsClick() {
    this.context.router.push('settings');
  }

  handleHomeClick() {
    if (this.context.router.location.pathname === '/') {
      // We are on the home page, just close the drawer.
      this.setState({drawerIsOpen: false});
    } else {
      this.context.router.push('/');
    }
  }

  handleAboutClick() {

  }

  handleGroceryClick() {

  }

  render() {
    let {title, overflowItems, backLink} = this.props;

    const overflowMenu = (
      <IconMenu
        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
      >
        {overflowItems}
        <MenuItem primaryText="Settings" onClick={this.handleSettingsClick.bind(this)} />
        <MenuItem primaryText="Logout" onClick={this.handleLogoutClick.bind(this)} />
      </IconMenu>
    );

    return (
      <div>
        <AppBar
          title={title ? title : "Trecipes"}
          iconElementRight={overflowMenu}
          iconElementLeft={backLink ? <IconButton><ArrowBack onClick={() => this.context.router.goBack()}/></IconButton> : null}
          onLeftIconButtonTouchTap={this.handleLeftIconClick.bind(this)}
        />
        <Drawer
          docked={false}
          width={300}
          open={this.state.drawerIsOpen}
          onRequestChange={(drawerIsOpen) => this.setState({drawerIsOpen})}
          containerStyle={{
            height: 'calc(100% - 64px)',
            top: '64px'
          }}
        >
          <div className="accountHeader">
            <div className="accountHeader-title">{this.props.profile.curUser.attributes.name}</div>
            <div className="accountHeader-subtitle">{this.props.profile.curUser.attributes.email}</div>
          </div>
          <MenuItem leftIcon={<HomeIcon />} onTouchTap={this.handleHomeClick.bind(this)}>Home</MenuItem>
          <MenuItem leftIcon={<GroceryIcon />} onTouchTap={this.handleGroceryClick.bind(this)}>Grocery List</MenuItem>
          <Divider style={{marginTop: '5px', marginBottom: '5px'}} />
          <MenuItem leftIcon={<LogoutIcon />} onTouchTap={this.handleLogoutClick.bind(this)}>Logout</MenuItem>
          <MenuItem leftIcon={<SettingsIcon />} onTouchTap={this.handleSettingsClick.bind(this)} >Settings</MenuItem>
          <MenuItem leftIcon={<InfoIcon />} onTouchTap={this.handleAboutClick.bind(this)}>About</MenuItem>
        </Drawer>
      </div>
    );
  }
};

Navigation.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps(state) {
  return {
    profile: state.profile
  };
}

export default connect(mapStateToProps, {...profileActions})(Navigation);
