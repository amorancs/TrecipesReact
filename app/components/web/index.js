import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {request} from 'axios';
import {Router, browserHistory} from 'react-router';
import Parse from 'parse';
import injectTapEventPlugin from 'react-tap-event-plugin';

import {parseAppId, parseServerUrl} from '../../constants/api';
import reducers from '../../reducers';
import restMiddleware from '../../middleware/rest';
import ApplicationRoutes from '../../routes';

const createStoreWithMiddleware = applyMiddleware(thunk, restMiddleware(request))(createStore);

// Parse backend
Parse.initialize(parseAppId);
Parse.serverURL = parseServerUrl;

// onTouchTap for material-ui
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

render( 
	<Provider store={createStoreWithMiddleware(reducers)}>
		<Router 
			onUpdate={() => window.scrollTo(0, 0)} 
			routes={ApplicationRoutes} 
			history={browserHistory} />
  </Provider>, 
  document.getElementById('root')
);