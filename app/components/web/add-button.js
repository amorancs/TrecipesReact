import React, {Component, PropTypes} from 'react';
import { SpeedDial, BubbleList, BubbleListItem } from 'react-speed-dial';
import Avatar from 'material-ui/Avatar';
import { blue500 } from 'material-ui/styles/colors';

import CameraIcon from 'material-ui/svg-icons/image/photo-camera';
import InternetIcon from 'material-ui/svg-icons/action/language';
import CreateIcon from 'material-ui/svg-icons/content/create';

import './add-button.css';

class AddButton extends Component {

  handleURLClick() {
    this.context.router.push('/add-by-url');
  }

  handleManualClick() {
    this.context.router.push('/add-manually');
  }

  handleImageClick() {
    this.context.router.push('/add-by-image');
  }

  render() {

    return (
      <div className="addButtonWrapper">
        <SpeedDial>
          <BubbleList>
            <BubbleListItem 
              key="AddbyURL"
              primaryText="Add by URL" 
              rightAvatar={<Avatar backgroundColor={blue500} icon={<InternetIcon />} />}
              onTouchTap={this.handleURLClick.bind(this)} 
            />
            <BubbleListItem
              key="AddbyImage"
              primaryText="Add by Image"
              rightAvatar={<Avatar backgroundColor={blue500} icon={<CameraIcon />} />}
              onTouchTap={this.handleImageClick.bind(this)}
            />
            <BubbleListItem
              key="AddManually"
              primaryText="Add Manually"
              rightAvatar={<Avatar backgroundColor={blue500} icon={<CreateIcon />} />}
              onTouchTap={this.handleManualClick.bind(this)}
            />
          </BubbleList>
        </SpeedDial>
      </div>
    );
  }
};

AddButton.contextTypes = {
  router: PropTypes.object
};

export default AddButton;
