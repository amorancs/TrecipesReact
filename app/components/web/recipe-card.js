import React from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Chip from 'material-ui/Chip';

import './recipe-card.css';

let recipeCard = (props) => {
  let {objectId, title, description, source, photo_url, categories, time} = props;
  let curYield = props.yield;
  let categoryChips;
  let timeTotal = (time ? time.total + ' minutes, ' + curYield : '');
  let imageHeight = (source ? '93px' : '77px')

  if (categories) {
    categoryChips = categories.map((category) => {
      return (
        <Chip key={category} style={{margin: 4, float: 'left'}}>
          {category}
        </Chip>
      );
    });
  }

  return (
    <div className="cardWrapper">
      <Card
        onClick={props.onClick}
      >
        <CardMedia
          overlay={<CardTitle title={title} subtitle={source} />}
          style={{minHeight: imageHeight}}
        >
          <img src={photo_url} />
        </CardMedia>
        <CardTitle subtitle={timeTotal} />
        <CardText>
          {description}
        </CardText>
        <CardActions style={{display: 'flex', flexWrap: 'wrap'}}>
          {categoryChips}
        </CardActions>
      </Card>

    </div>
  )
};

export default recipeCard;
