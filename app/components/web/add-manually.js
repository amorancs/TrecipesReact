import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import FlatButton from 'material-ui/FlatButton';

import Navigation from './navigation';
import * as parserActions from '../../actions/parser';
import * as status from '../../constants/statuses';
import './add-manually.css';

const defaultState = {
  isLoading: false,
  isError: false
};

class AddManually extends Component {

  componentWillMount() {
    this.setState(defaultState);
  }

  componentWillReceiveProps(nextProps) {

  }
  
  handleSave(values) {
    console.log('Search URL: ' + values.url);
    // this.props.fetchRecipeAtUrl(values.url);
    this.setState({isLoading: true});
  }

  render() {
    return (
      <div>
        <Navigation title="Add Recipe Manually" backLink={true} />
        Add Manually (Form) TODO
        {this.state.isLoading ? 'Loading...' :  null}
        {this.state.isError ? 'Error!' : null}
        <div className="addManuallyForm-wrapper">
          <form onSubmit={this.props.handleSubmit(this.handleSave.bind(this))}>
            <div>
              <Field 
                name="title" 
                component={TextField}
                hintText="Title"
                floatingLabelText="Title" 
                type="text"/>
            </div>
            <FlatButton 
              label="Save" 
              primary={true}
              type="submit" />
          </form>
        </div>
      </div>
    );
  }
}

AddManually.propTypes = {
  handleSubmit: PropTypes.func.isRequired
};

AddManually.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps(state) {
  return {
    parser: state.parser
  };
}

export default connect(mapStateToProps, {...parserActions})(reduxForm({form: 'addManually'})(AddManually));
