import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import CircularProgress from 'material-ui/CircularProgress';

import Navigation from './navigation';
import RecipeCard from './recipe-card';
import AddButton from './add-button';
import * as recipeActions from '../../actions/recipes';
import './home.css';

class Home extends Component {

  componentWillMount() {
    if (!this.props.profile.curUser) {
      this.context.router.push('login');
    } else {
      this.props.getRecipes();
    }
  }

  onRecipeCardClick(objectId) {
    this.context.router.push(`/recipe?objectId=${objectId}`);
  }

  render() {
    if (this.props.recipes.recipeList.length <= 0) {
      return (
        <div className="progressWrapper">
          <CircularProgress size={60} thickness={7}/>
        </div>
      );
    }

    let cardList = [];
    this.props.recipes.recipeList.map((recipe) => {
      cardList.push(
        <RecipeCard
          key={recipe.objectId}
          {...recipe}
          onClick={this.onRecipeCardClick.bind(this, recipe.objectId)}
        />
      );
    });

    return (
      <div>
        <Navigation />
        {cardList}
        <div className="cardListBottomSpacer">&nbsp;</div>
        <AddButton />
      </div>
    );
  }
}

Home.propTypes = {
  getRecipes: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  recipes: PropTypes.object.isRequired
};

Home.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps(state) {
  return {
    profile: state.profile,
    recipes: state.recipes
  };
}

export default connect(mapStateToProps, {...recipeActions})(Home);
