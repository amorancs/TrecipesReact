import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import FlatButton from 'material-ui/FlatButton';
import Dropzone from 'react-dropzone';
import RaisedButton from 'material-ui/RaisedButton';

import Navigation from './navigation';
import * as parserActions from '../../actions/parser';
import * as status from '../../constants/statuses';
import './add-by-image.css';

const defaultState = {
  isLoading: false,
  isError: false
};

class AddByImage extends Component {

  componentWillMount() {
    this.setState(defaultState);
  }

  componentWillReceiveProps(nextProps) {

  }

  renderDropzoneInput(field) {
    const files = field.input.value;
    return (
      <div>
        <Dropzone
          name={field.name}
          style={{margin: '5px'}}
          multiple={false}
          accept={'image/*'}
          onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        >
          <RaisedButton label="Choose Image" />
        </Dropzone>
        {field.meta.touched && field.meta.error &&
          <span className="error">{field.meta.error}</span>}
        {files && Array.isArray(files) && files[0].name}
      </div>
    );
  }
  
  handleSearch(values) {
    this.setState({isLoading: true});
    this.props.parseImageForRecipe(values.image[0]);
  }

  render() {
    return (
      <div>
        <Navigation title="Add Recipe by Image" backLink={true} />
        {this.state.isLoading ? 'Loading...' :  null}
        {this.state.isError ? 'Error!' : null}
        <div className="addByImageForm-wrapper">
          <form onSubmit={this.props.handleSubmit(this.handleSearch.bind(this))}>
            <div>
              <Field
                name="image"
                component={this.renderDropzoneInput}
              />
            </div>
            <FlatButton 
              label="Parse" 
              primary={true}
              type="submit" />
          </form>
        </div>
      </div>
    );
  }
}

AddByImage.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

AddByImage.contextTypes = {
  router: PropTypes.object
};

function mapStateToProps(state) {
  return {
    parser: state.parser
  };
}

export default connect(mapStateToProps, {...parserActions})(reduxForm({form: 'addByImage'})(AddByImage));
