import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import _throttle from 'lodash/throttle';

import * as profileActions from '../../actions/profile';
import './main.css';

const CLASS_PREFIX = 'layout-';
const breakpoints = {
  0: 'xxs',
  414: 'xs',
  568: 'sm',
  768: 'md',
  1024: 'lg',
  1280: 'xl',
  1440: 'xxl',
  1600: 'xxxl'
};

class App extends Component {

  componentWillMount() {
    this.props.retrieveCachedUser();
    this.setState({rootGridClassList: []});
  }

  getGridLayoutClasses(el) {
    let result = [];

    if (el && typeof el.offsetWidth === 'number') {
      let width = el.offsetWidth;

      for (let breakpointSize in breakpoints) {
        if (width > Number(breakpointSize)) {
          result.push(CLASS_PREFIX + breakpoints[breakpointSize]);
        }
      }
    }
    return result;
  }

  setGridLayoutClasses() {
    let rootNode = ReactDOM.findDOMNode(this.refs.pageRoot);

    this.setState({rootGridClassList: this.getGridLayoutClasses(rootNode)});
  }

  resize() {
    if (!this.lastWindowWidth) {
      this.lastWindowWidth = 0;
    }

    let currentWindowWidth = window.innerWidth;
    if (currentWindowWidth === this.lastWindowWidth) {
      return;
    }

    this.lastWindowWidth = currentWindowWidth;

    this.setGridLayoutClasses();
  }

  componentDidMount() {
    this.setGridLayoutClasses();

    this._resizeThrottleFunc = _throttle(this.resize.bind(this), 200);
    window.addEventListener('resize', this._resizeThrottleFunc);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._resizeThrottleFunc);
  }

  render() {
    return (
      <MuiThemeProvider>
        <div ref="pageRoot" className={'pageRoot ' + this.state.rootGridClassList.join(' ')} >
          {this.props.children}
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.object,
  retrieveCachedUser: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    profile: state.profile
  };
}

export default connect(mapStateToProps, {...profileActions})(App);
