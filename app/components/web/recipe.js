import React, {Component} from 'react';
import {connect} from 'react-redux';

import Navigation from './navigation';
import * as recipeActions from '../../actions/recipes';
import './recipe.css';

class Recipe extends Component {

  renderIngredients(ingredientsObjList) {
    let ingredientsElems = [];
    ingredientsObjList.forEach((ingredientsObj) => {
      ingredientsElems.push(
        <div>
          <div>{ingredientsObj.name}</div>
          <ul>{ingredientsObj.list.map((ingredient) => {
            return <li>{ingredient}</li>
          })}
          </ul>
        </div>
      );
    });

    return ingredientsElems;
  }

  renderInstructions(instructionsObjList) {
    let instructionsElems = [];
    instructionsObjList.forEach((instructionsObj) => {
      instructionsElems.push(
        <div>
          <div>{instructionsObj.name}</div>
          <ol>{instructionsObj.list.map((instruction) => {
            return <li>{instruction}</li>
          })}
          </ol>
        </div>
      );
    });

    return instructionsElems;
  }

  render() {
    let {objectId} = this.props.location.query;

    let recipe = this.props.recipes.recipeList.find((curRecipe) => {
      return curRecipe.objectId == objectId;
    });

    console.log(recipe);

    return (
      <div>
        <Navigation title={recipe.title} backLink={true} />
        <img className="recipe-photo" src={recipe.photo_url} />
        <div>Description: {recipe.description}</div>
        <div>Notes: {recipe.notes}</div>
        <div>{this.renderIngredients(recipe.ingredients)}</div>
        <div>{this.renderInstructions(recipe.instructions)}</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    recipes: state.recipes
  };
}

export default connect(mapStateToProps, {...recipeActions})(Recipe);
