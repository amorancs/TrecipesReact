import React, {Component, PropTypes} from 'react';
import Navigation from './navigation';

import './settings.css';

class Settings extends Component {

  render() {

    return (
      <div>
        <Navigation title={'Settings'} backLink={true} />
        Settings Page  
      </div>
    );
  }
};

Settings.contextTypes = {
  router: PropTypes.object
};

export default Settings;
