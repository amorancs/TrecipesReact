export function jsonToParseObj(json, parseObj) {
  for (let key in json) {
    
    // skip loop if the property is from prototype
    if (!json.hasOwnProperty(key)) {
      continue;
    }

    let value = json[key];
    parseObj.set(key, value);  
  }
  return;
}
