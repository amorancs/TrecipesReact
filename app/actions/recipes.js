import Parse from 'parse';
import {
    GET_RECIPES_REQUEST, 
    GET_RECIPES_SUCCESS, 
    GET_RECIPES_ERROR,
    SAVE_RECIPE_REQUEST,
    SAVE_RECIPE_SUCCESS,
    SAVE_RECIPE_ERROR
} from './types';
import {jsonToParseObj} from '../util/util';

export function getRecipes() {
  let Query = Parse.Object.extend('Recipe_v2');
  let query = new Parse.Query(Query);
    
  return (dispatch) => {
    dispatch({type: GET_RECIPES_REQUEST});

    return query.find({
      success: (results) => dispatch({
        type: GET_RECIPES_SUCCESS,
        payload: results
      }),
      error: (error) => dispatch({
        type: GET_RECIPES_ERROR,
        error: error
      })
    });
  };
}

export function saveRecipe(sample) {
  let Recipe = Parse.Object.extend('Recipe_v2');
  let recipe = new Recipe();
  jsonToParseObj(sample, recipe);
    // recipe.set("testkey", "testvalue");

  return (dispatch) => {
    dispatch({type: SAVE_RECIPE_REQUEST});

    return recipe.save({
      success: (result) => dispatch({
        type: SAVE_RECIPE_SUCCESS,
        payload: result.id
      }),
      error: (result, error) => dispatch({
        type: SAVE_RECIPE_ERROR,
        error: error
      })
    });
  };
}
