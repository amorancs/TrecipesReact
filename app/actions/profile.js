import Parse from 'parse';
import {
  LOGIN_REQUEST, 
  LOGIN_SUCCESS, 
  LOGIN_ERROR,
  CACHED_USER,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS
} from './types';

export function processLogin(username, password) {
  return (dispatch) => {
    dispatch({type: LOGIN_REQUEST});

    return Parse.User.logIn(username, password, {
      success: (user) => dispatch({
        type: LOGIN_SUCCESS,
        payload: user
      }),
      error: (user, error) => dispatch({
        type: LOGIN_ERROR,
        error: error
      })
    });
  };
}

export function retrieveCachedUser() {
  return (dispatch) => {
    let currentUser = Parse.User.current();
    if (currentUser) {
      dispatch({
        type: CACHED_USER,
        payload: currentUser
      });
    }
  };
}

export function processLogout() {
  return (dispatch) => {
    dispatch({type: LOGOUT_REQUEST});

    Parse.User.logOut().then(() => {
      dispatch({
        type: LOGOUT_SUCCESS,
      });
    });
  };
}
