import Parse from 'parse';
import axios from 'axios';
import {
    FETCH_URL_REQUEST, 
    FETCH_URL_SUCCESS, 
    FETCH_URL_ERROR,
    PARSE_IMAGE_REQUEST,
    PARSE_IMAGE_SUCCESS,
    PARSE_IMAGE_ERROR
} from './types';
import {jsonToParseObj} from '../util/util';

const FETCH_RECIPE_API = 'https://recipes-webparser.herokuapp.com/api/parse?url=';
const PARSE_IMAGE_API = 'https://recipes-textparser.herokuapp.com/parse';
const PARSE_INGREDIENT_API = 'https://recipes-ingredients.herokuapp.com/api/parse-ingredient';

export function parseImageForRecipe(image) {
  let body = new FormData();
  body.append('recipe_image', image);
  let recipeObj;

  return (dispatch) => {
    dispatch({type: PARSE_IMAGE_REQUEST});

    axios.post(PARSE_IMAGE_API, body)
      .then(function (recipeResponse) {
        recipeObj = {
          title: recipeResponse.data.title,
          ingredients: [{
            name: '',
            list: recipeResponse.data.ingredients
          }],
          instructions: [{
            name: '',
            list: recipeResponse.data.instructions
          }],
          source: 'Trecipes Image Parser'
        }

        return axios.request({
          url: PARSE_INGREDIENT_API,
          method: 'post',
          data: {ingredients: recipeObj.ingredients}
        });

      })
      .then((parsedIngredientResponse) => {

        // Replace ingredients array with parsed ingredients array.
        recipeObj.ingredients = parsedIngredientResponse.data;

        // Create ParseObject and save to Parse Backend
        let Recipe = Parse.Object.extend('Recipe_v2');
        let recipe = new Recipe();
        jsonToParseObj(recipeObj, recipe);

        recipe.setACL(new Parse.ACL(Parse.User.current()));

        return recipe.save({
          success: (result) => dispatch({
            type: PARSE_IMAGE_SUCCESS,
            payload: result.id
          }),
          error: (result, error) => dispatch({
            type: PARSE_IMAGE_ERROR,
            error: error
          })
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

export function fetchRecipeAtUrl(url) {
  let recipeObj;

  return (dispatch) => {
    dispatch({type: FETCH_URL_REQUEST});
    
    axios.get(FETCH_RECIPE_API + url)
      .then((recipeResponse) => {
        recipeObj = recipeResponse.data;

        let ingredientObj = recipeObj.ingredients;

        // Parse all ingredients in ingredients array.
        return axios.request({
          url: PARSE_INGREDIENT_API,
          method: 'post',
          data: {ingredients: ingredientObj}
        });
      })
      .then((parsedIngredientResponse) => {
        console.log(parsedIngredientResponse);

        // Replace ingredients array with parsed ingredients array.
        recipeObj.ingredients = parsedIngredientResponse.data;

        // Create ParseObject and save to Parse Backend
        let Recipe = Parse.Object.extend('Recipe_v2');
        let recipe = new Recipe();
        jsonToParseObj(recipeObj, recipe);

        recipe.setACL(new Parse.ACL(Parse.User.current()));

        return recipe.save({
          success: (result) => dispatch({
            type: FETCH_URL_SUCCESS,
            payload: result.id,
            url: url
          }),
          error: (result, error) => dispatch({
            type: FETCH_URL_ERROR,
            error: error,
            url: url
          })
        });
      })
      .catch((error) => {
        console.log(error);
        dispatch({type: FETCH_URL_ERROR, url: url});
      })
  }
}
