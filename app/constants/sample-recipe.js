/* eslint-disable quotes, camelcase */
const sample = {
  catIds: [],
  credits: "",
  description: "",
  ingredients: [
    {
      list: [
        "1 sleeve saltine crackers",
        "1 cup butter",
        "1 cup sugar (can use brown sugar or y of each)",
        "12 oz semi sweet chocolate chips",
        "8 oz peanut butter chips",
        "optional: 1 cup chopped nuts (pecans, walnuts or aimonds)"
      ],
      name: "",
      processedList: [
        {
          ingredient: "sleeve saltine crackers",
          measurements: [{amount: "1", unit: ""}]
        },
        {
          ingredient: "butter",
          measurements: [{amount: "1", unit: "cup"}]
        },
        {
          ingredient: "sugar (can use brown sugar or y of each)",
          measurements: [{amount: "1", unit: "cup"}]
        },
        {
          ingredient: "semi sweet chocolate chips",
          measurements: [{amount: "12", unit: "oz"}]
        },
        {
          ingredient: "peanut butter chips",
          measurements: [{amount: "8", unit: "oz"}]
        },
        {
          ingredient: "optional: 1 cup chopped nuts (pecans, walnuts or aimonds)",
          measurements: []
        }
      ]
    }
  ],
  instructions: [
    {
      list: [
        "Preheat oven to 400 degreesUsing a rimmed baking sheet, lay out crackers in onelayer until sheet is completely covered.",
        "In a medium sized saucepan melt the butter and sugar.",
        "Bring to a full rolling boil stirring often. Once the mixturecomes to a full rolling boil, set timer for 3 mins.",
        "Pour mixture over saltines and bake for 5 mins. Will bereally bubblyRemove baking sheet from oven and immediately pourchoc chips on top. Allow pan to sit for a few moments sothe chips soften. Use knife to spread choc mixture overtop. Peanut butter chips will just blend in with thechocolate. If using nuts, sprinkle on top.",
        "Freeze for 20-25 mins (or refrigerate until hardened)Break toffee into pieces and store in airtight container.",
        "Freezes well and maintains crispness.",
        "Can vary choc chips and even try white choc. Addingcandypieces on top instead of nuts is also tasty!",
        "May consider lining baking sheet with foil for easy removalthough I have never had any trouble getting the candy off ofthe pan after freezing."
      ],
      name: ""
    }
  ],
  notes: "",
  photo_url: "",
  source: "",
  status: "",
  time: {
    cook: 0,
    prep: 0,
    total: 0
  },
  title: "Saltine Cracker Toffee Candy",
  url: "",
  yield: ""
};
/* eslint-enable quotes, camelcase */

export default sample;
