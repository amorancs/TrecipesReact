import React from 'react';
import {Route, IndexRoute} from 'react-router';

import Main from './components/web/main';
import Home from './components/web/home';
import Login from './components/web/login';
import Recipe from './components/web/recipe';
import AddByUrl from './components/web/add-by-url';
import AddManually from './components/web/add-manually';
import AddByImage from './components/web/add-by-image';
import Settings from './components/web/settings';

export default (
  <Route path="/" component={Main} >
    <IndexRoute component={Home} />
    <Route path="login" component={Login} />
    <Route path="settings" component={Settings} />
    <Route path="recipe" component={Recipe} />
    <Route path="add-by-url" component={AddByUrl} />
    <Route path="add-manually" component={AddManually} />
    <Route path="add-by-image" component={AddByImage} />
  </Route>
);
