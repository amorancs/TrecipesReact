var webpack = require('webpack');

var config = {
  entry: [
    'webpack/hot/only-dev-server',
    './app/components/web/index.js'
  ],
  output: {
    path: __dirname + '/web/public',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  module : {
    loaders : [
      {
        test : /\.js$/,
        include : /app/,
        loaders : ['react-hot', 'babel-loader?presets[]=es2015,presets[]=react']
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ]
  },
  devServer: {
    hot: true,
    inline: true,
    contentBase: './web/public'
  }
};

module.exports = config;