import React, { Component } from 'react';
import {AppRegistry} from 'react-native';
import Main from './app/components/native/main';

export default class TrecipesReact extends Component {
  render() {
    return (
      <Main />
    );
  }
}

AppRegistry.registerComponent('TrecipesReact', () => TrecipesReact);
